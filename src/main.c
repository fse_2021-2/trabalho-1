#include "control.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char * argv[]) {
    int command;

    do {
        system("clear");

        printf("===== MENU PRINCIPAL =====\n\n");
        printf("(1) Configurar Sistema\n");
        printf("(2) Iniciar Sistema\n");
        printf("(0) Encerrar Programa\n\n");
        printf("Comando: ");
        scanf("%d", &command);
        setbuf(stdin, NULL);

        switch (command) {
            case 1:
                control_system_configuration();
                break;
            case 2:
                control_system_init();
                break;
            case 0:
                printf("Encerrando programa...\n");
                break;
            default:
                printf("Comando inválido, digite novamente...\n");
                sleep(1);
                break;
        }
    } while(command != 0);

    return 0;
}