#include "uart.h"

#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stddef.h>

static int uart0_filestream = -1;

void uart_init() {
    uart0_filestream = open("/dev/serial0", O_RDWR | O_NDELAY | O_NOCTTY);
    if (uart0_filestream == -1) {
        printf("Erro na abertura da UART\n");
    } else {
        printf("UART inicializada!\n");
    }

    struct termios options;
    tcgetattr(uart0_filestream, &options);
    options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
    options.c_iflag = IGNPAR;
    options.c_oflag = 0;
    options.c_lflag = 0;
    tcflush(uart0_filestream, TCIFLUSH);
    tcsetattr(uart0_filestream, TCSANOW, &options);
}

void uart_write(unsigned char * tx_buffer, size_t tx_size) {
    if (uart0_filestream != -1) {
        write(uart0_filestream, tx_buffer, tx_size);
    }
}

void uart_read(unsigned char * rx_buffer, size_t rx_size) {
    if (uart0_filestream != -1) {
        usleep(400 * 1000); // Espera 400ms antes de ler a mensagem pela 1º vez

        // Tenta ler a mensagem por até 5 vezes
        for (int i = 0; i < 5; i++) {
            int rx_length = read(uart0_filestream, (void*)rx_buffer, rx_size);

            if (rx_length > 0) break; // Mensagem lida

            // Caso a mensagem não seja lida, espera 100ms e tenta ler novamente
            usleep(100 * 1000); 
        }
    }
}

void uart_close() {
    printf("\nComunicação UART encerrada!\n");
    close(uart0_filestream);
}
