#include "uart_communication.h"
#include "modbus.h"
#include "uart.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define SERVER_CODE 0x01

#define REQUEST_DATA_CODE 0x23
#define REQUEST_INTERN_TEMPERATURE_CODE 0xC1
#define REQUEST_POTENTIOMENTER_TEMPERATURE_CODE 0xC2
#define REQUEST_USER_COMMAND_CODE 0xC3

#define SEND_DATA_CODE 0x16
#define SEND_CONTROL_SIGNAL_CODE 0xD1
#define SEND_REFERENCE_SIGNAL_CODE 0xD2
#define SEND_SYSTEM_STATE_CODE 0xD3
#define SEND_CONTROL_MODE_CODE 0xD4

/* Comando a ser enviado na operação de escrita de dados */
static char command[256];

/* Dado recebido na operação de leitura de dados */
static char received_data[10];

void uart_communication_init() {
    uart_init();
    send_system_state(0);
}

void command_init(int code, int sub_code) {
    memset(command, 0, 256);

    command[0] = SERVER_CODE;
    command[1] = code;
    command[2] = sub_code;

    // Matrícula
    command[3] = 3;
    command[4] = 0;
    command[5] = 3;
    command[6] = 4;
}

float request_intern_temperature() {
    float intern_temperature;

    printf("Solicitando a temperatura interna ...\n");
    command_init(REQUEST_DATA_CODE, REQUEST_INTERN_TEMPERATURE_CODE);

    send_command_n_read_data(7);
    memcpy(&intern_temperature, received_data, 4);

    return intern_temperature;
}

float request_potentiometer_temperature() {
    float potentiometer_temperature;

    printf("Solicitando a temperatura do potenciômetro ...\n");
    command_init(REQUEST_DATA_CODE, REQUEST_POTENTIOMENTER_TEMPERATURE_CODE);

    send_command_n_read_data(7);
    memcpy(&potentiometer_temperature, received_data, 4);

    return potentiometer_temperature;
}

int request_user_command() {
    int received_command;

    printf("Lendo comandos do usuário ...\n");
    command_init(REQUEST_DATA_CODE, REQUEST_USER_COMMAND_CODE);
    
    send_command_n_read_data(7);
    memcpy(&received_command, received_data, 4);

    return received_command;
}

void send_control_signal(int control_signal) {
    printf("Enviando sinal de controle ...\n");
    command_init(SEND_DATA_CODE, SEND_CONTROL_SIGNAL_CODE);

    memcpy(&command[7], &control_signal, 4);
    modbus_write(command, 11);
}

void send_reference_temperature(float reference_temperature) {
    printf("Enviando temperatura de referência ...\n");
    command_init(SEND_DATA_CODE, SEND_REFERENCE_SIGNAL_CODE);

    memcpy(&command[7], &reference_temperature, 4);
    modbus_write(command, 11);
}

void send_system_state(char byte) {
    printf("Mudando o estado sistema ...\n");
    command_init(SEND_DATA_CODE, SEND_SYSTEM_STATE_CODE);

    command[7] = byte;
    send_command_n_read_data(8);
}

void send_control_mode(char byte) {
    printf("Mudando modo de controle ...\n");
    command_init(SEND_DATA_CODE, SEND_CONTROL_MODE_CODE);

    command[7] = byte;
    send_command_n_read_data(8);
}

void send_command_n_read_data(int command_size) {
    /* Tenta a comunicação (write + read) por até 3 vezes para contornar
        possíveis problemas na leitura da mensagem */
    for (int i = 0; i < 3; i++) {
        modbus_write(command, command_size);
        if (modbus_read(command, received_data)) {
            return; // Mensagem lida e validada
        }
    }

    // Encerra o programa caso não consiga ler a mensagem em 3 tentativas
    printf("\nProblema na comunicação via UART\n");
    exit(1);
}

void uart_communication_close() {
    send_system_state(0);
    uart_close();
}
