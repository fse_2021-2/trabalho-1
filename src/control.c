#include "control.h"
#include "uart_communication.h"
#include "gpio_power_circuit.h"
#include "i2c_connection.h"
#include "csv.h"

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>

/* Modo de controle da temperatura de referência. Default: Potenciômetro */
enum reference_temperature_mode control_mode = POTENTIOMETER;

/* Estado do sistema. Default: Desligado */
enum system_state system_state = OFF;

/* Struct com os dados da curva de reflow */
Reflow * reflow_curve;

/* Arquivo csv com o log de cada execução do sistema */
FILE * data_file;

/* Contador da curva de Reflow */
static int reflow_count = 0;

/* Constantes PID -> Default: Valores da Rasp43 */
static float kp = 20.0, ki = 0.1, kd = 100.0;

/* TR: Temperatura de Referência, TI: Temperatura Interna, TA: Temperatura Ambiente */ 
static float tr, ti, ta;

void set_up_modules() {
    uart_communication_init();
    gpio_power_circuit_init();
    i2c_connection_init();

    signal(SIGINT, set_down_modules);
}

void set_down_modules() {
    printf("\n\nComando SIGINT recebido, encerrando comunicações ....\n");

    gpio_power_circuit_close();
    i2c_connection_close();
    uart_communication_close();

    exit(0); // Encerra o programa
}

void control_system_init() {
    printf("Sistema iniciado no modo %s\n\n", get_actual_control_mode_name());

    set_up_modules();
    configure_pid_constants(kp, ki, kd);
    reflow_curve = read_csv();
    
    if (control_mode == POTENTIOMETER) send_control_mode(0);
    else send_control_mode(1);

    control_system_start();
}

void control_system_start() {
    do {
        int user_command = request_user_command();

        switch (user_command) {
            case 1:
                system_state = ON;
                send_system_state(1);
                printf("Sistema ligado\n\n");

                // Cria um arquivo cada vez que o sistema é ligado
                data_file = create_csv_file();
                break;
            case 2:
                system_state = OFF;
                send_system_state(0);
                printf("Sistema desligado\n\n");

                gpio_power_circuit_close();
                fclose(data_file);
                break;
            case 3:
                control_mode = POTENTIOMETER;
                send_control_mode(0);
                printf("Modo Potenciômetro ativado\n\n");
                break;
            case 4:
                reflow_count = 0;
                control_mode = REFLOW;
                send_control_mode(1);
                printf("Modo Curva de Reflow ativado\n\n");
                break;
            case 0:
            default:
                break;
        }

        control_system_routine();

        /* Como já há delay entre as diversas comunicações, optei por retirar
            o delay de 1s entre a leitura de comandos do usuário */
        // sleep(1); 
    } while(1);
}

void control_system_routine() {
    if (system_state == ON) {
        ta = get_ambient_temperature();
        ti = request_intern_temperature();

        if (control_mode == POTENTIOMETER) {
            tr = request_potentiometer_temperature();
        } else {
            if (control_mode == REFLOW) {
                tr = get_reflow_curve_temperature();
                reflow_count++;
            }
            send_reference_temperature(tr);
        }
        
        int control_signal = calculate_control_signal(tr, ti);
        power_circuit_control(control_signal);
        send_control_signal(control_signal);

        write_csv_row(data_file, get_actual_control_mode_name(), tr, ti, ta, control_signal);
        lcd_show_measured_data(get_actual_control_mode_name(), tr, ti, ta);

        printf("MODO: %s, TR: %.2f, TI: %.2f, TA: %.2f, SINAL: %d\n\n", get_actual_control_mode_name(), tr, ti, ta, control_signal);
    } else {
        lcd_show_system_state_off();
    }
}

float get_reflow_curve_temperature() {
    int reflow_curve_size = 10;

    for (int i = 0; i < reflow_curve_size - 1; i++) {
        if (reflow_count < reflow_curve[i + 1].time) {
            return reflow_curve[i].temperature;
        }
    }

    return reflow_curve[reflow_curve_size - 1].temperature;
}

void control_system_configuration() {
    int command;

    do {
        system("clear");

        printf("========== Configuração de variáveis do sistema ==========\n\n");
        printf("(1) Alterar modo de temperatura de referência -> Atual: %s\n", get_actual_control_mode_name());
        printf("(2) Alterar constantes PID -> Atual: (Kp: %.1f, Ki: %.1f, Kd: %.1f)\n", kp, ki, kd);
        printf("(0) Voltar para o menu principal\n\n");
        printf("Comando: ");
        scanf("%d", &command);
        setbuf(stdin, NULL);

        switch (command) {
            case 1:
                user_set_control_mode();
                break;
            case 2:
                user_set_pid_constants();
                break;
            case 0:
                break;
            default:
                printf("\nComando inválido, digite novamente...");
                sleep(1);
                break;
        }
    } while(command != 0);
}

void user_set_control_mode() {
    int command;

    do {
        system("clear");

        printf("\n===== Modo da Temperatura de Referência =====\n\n");
        printf("(1) Potênciometro Externo\n");
        printf("(2) Curva de Reflow\n");
        printf("(3) Entrada via Terminal\n\n");
        printf("Comando: ");
        scanf("%d", &command);
        control_mode = command;

        switch (control_mode) {
            case POTENTIOMETER:
            case REFLOW:
                break;
            case TERMINAL:
                printf("\nDigite o valor da temperatura de referência: ");
                scanf("%f", &tr);
                break;
            default:
                printf("\nComando inválido, digite novamente...");
                sleep(1);
                break;
        }

    } while(control_mode != POTENTIOMETER && control_mode != REFLOW && control_mode != TERMINAL);
}

void user_set_pid_constants() {
    printf("\nKp: ");
    scanf("%f", &kp);
    printf("Ki: ");
    scanf("%f", &ki);
    printf("Kd: ");
    scanf("%f", &kd);
}

char * get_actual_control_mode_name() {
    switch (control_mode) {
        case POTENTIOMETER:
            return "POTENC.";
        case REFLOW:
            return "REFLOW";
        case TERMINAL:
            return "TERMINAL";
        default:
            return "";
    }
}
