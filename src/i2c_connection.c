#include "i2c_connection.h"
#include "sensor_bme280.h"
#include "lcd_display.h"

#define LCD_LINE_1  0x80
#define LCD_LINE_2  0xC0

void i2c_connection_init() {
    sensor_bme280_init();
    lcd_display_init();
}

void i2c_connection_close() {
    lcd_display_clear();
}

float get_ambient_temperature() {
    return sensor_bme280_ambient_temperature();
}

void lcd_show_system_state_off() {
    lcd_display_clear();

    lcd_location(LCD_LINE_1);
    type_string("    Sistema     ");
    lcd_location(LCD_LINE_2);
    type_string("   Desligado!   ");
}

void lcd_show_measured_data(char * control_mode, float tr, float ti, float ta) {
    lcd_display_clear();

    lcd_location(LCD_LINE_1);
    type_string(control_mode);
    type_string(" TI:");
    type_float(ti);

    lcd_location(LCD_LINE_2);
    type_string("TA:");
    type_float(ta);
    type_string(" TR:");
    type_float(tr);
}
