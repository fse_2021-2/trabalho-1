#include "csv.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#define MAXSIZEROW 100

Reflow * read_csv() {
    int i = 0;
    char row[MAXSIZEROW];

    Reflow * reflow_curve = malloc(sizeof(Reflow));
    FILE * reflow_file = fopen("data/curva_reflow.csv", "r+b");

    fgets(row, MAXSIZEROW, reflow_file); // Lê a primeira linha do csv (cabeçalho)
    while(fgets(row, MAXSIZEROW, reflow_file) != NULL) {
        reflow_curve = realloc(reflow_curve, (i+1) * sizeof(Reflow));
        reflow_curve[i].time = atoi(strtok(row, ",")); 
        reflow_curve[i].temperature = atoi(strtok(NULL, ","));
        i++;
    }

    fclose(reflow_file);

    return reflow_curve;
}

FILE * create_csv_file() {
    time_t now;
    char file_name[100];

    snprintf(file_name, 100, "data/data_file_%ld.csv", time(&now));
    FILE * data_csv_file = fopen(file_name, "w");
    fprintf(data_csv_file, "Data e Hora, Modo de Controle, TR (ºC), TI (ºC), TA (ºC), Sinal de Controle\n");

    return data_csv_file;
}

void write_csv_row(FILE *data_file, char * control_mode_name, float tr, float ti, float ta, int control_signal) {
    time_t now;
    time(&now);
    struct tm *dt = localtime(&now);

    // Escreve a data no arquivo
    fprintf(data_file, "%d-%02d-%02d %02d:%02d:%02d, ", dt->tm_year + 1900,
        dt->tm_mon + 1, dt->tm_mday, dt->tm_hour, dt->tm_min, dt->tm_sec);
    
    fprintf(data_file, "%s, %.2f, %.2f, %.2f, %d\n", control_mode_name,
        tr, ti, ta, control_signal);
}
