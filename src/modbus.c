#include "modbus.h"
#include "crc16.h"
#include "uart.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <stddef.h>

/* Variável com a mensagem recebida */
static char received_message[256];

void modbus_write(char * command, size_t command_size) {
    short crc = calcula_CRC((unsigned char *)command, command_size);
    memcpy(&command[command_size], &crc, 2);
    uart_write((unsigned char *)command, command_size + 2);
}

bool modbus_read(char * sended_message, char * data) {
    int received_message_size = 9;
    memset(received_message, 0, 256);

    uart_read((unsigned char *)received_message, received_message_size);

    bool message_is_valid = validate_crc(received_message_size - 2) && validate_message_header(sended_message);
    if (!message_is_valid) {
        /* Caso a mensagem seja inválida (erro no CRC ou header)
            lê mais uma vez para limpar a fila de leitura e retorna falso */
        uart_read((unsigned char *)received_message, received_message_size);

        return false;
    }

    /* Caso a mensagem seja válida,
        copia a parte de dados para a variável 'data' e retorna true */
    memcpy(data, &received_message[3], 4);
    return true;
}

bool validate_crc(int commands_size) {
    short received_crc, message_crc;

    memcpy(&received_crc, &received_message[commands_size], 2);
    message_crc = calcula_CRC((unsigned char *)received_message, commands_size);

    if (message_crc != received_crc) { 
        printf("Erro no CRC recebido ... \n");
        return false;
    }

    return true;
}

bool validate_message_header(char * sended_message) {
    if ((received_message[1] != sended_message[1]) | (received_message[2] != sended_message[2])) {
        printf("Erro no header recebido ... \n");
        return false;
    }

    return true;
}
