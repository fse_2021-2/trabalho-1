#include "gpio_power_circuit.h"
#include "pid.h"

#include <wiringPi.h>
#include <softPwm.h>

#define PWM_RESISTOR_PIN 4
#define PWM_FAN_PIN 5

#define MAX(x, y) (((x) > (y)) ? (x) : (y))

void gpio_power_circuit_init() {
    wiringPiSetup();
}


void gpio_power_circuit_close() {
    pinMode(PWM_RESISTOR_PIN, INPUT);
    pinMode(PWM_FAN_PIN, INPUT);
}

void configure_pid_constants(float kp, float ki, float kd) {
    pid_configura_constantes(kp, ki, kd);
}

int calculate_control_signal(int tr, int ti) {
    int control_signal;

    pid_atualiza_referencia(tr);
    control_signal = pid_controle(ti);

    return control_signal;
}

void power_circuit_control(int control_signal) {
    if (control_signal >= 0) {
        pinMode(PWM_FAN_PIN, INPUT); // Desliga a Ventoinha
        pinMode(PWM_RESISTOR_PIN, OUTPUT); // Seta o Resistor para modo OUTPUT

        softPwmCreate(PWM_RESISTOR_PIN, 0, 100);
        softPwmWrite(PWM_RESISTOR_PIN, control_signal);
    } else {
        pinMode(PWM_RESISTOR_PIN, INPUT); // Desliga o Resistor
        pinMode(PWM_FAN_PIN, OUTPUT); // Seta a Ventoinha para modo OUTPUT

        softPwmCreate(PWM_FAN_PIN, 0, 100);
        softPwmWrite(PWM_FAN_PIN, MAX(40, control_signal * -1));
    }
}
