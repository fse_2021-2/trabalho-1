#include "lcd_display.h"

#include <wiringPiI2C.h>
#include <wiringPi.h>
#include <stdlib.h>
#include <stdio.h>

// Define some device parameters
#define I2C_ADDR   0x27 // I2C device address

// Define some device constants
#define LCD_CHR  1 // Mode - Sending data
#define LCD_CMD  0 // Mode - Sending command

#define LCD_BACKLIGHT   0x08  // On

#define ENABLE  0b00000100 // Enable bit

static int fd;  // seen by all subroutines

void lcd_display_init() {
    lcd_byte(0x33, LCD_CMD); // Initialise
    lcd_byte(0x32, LCD_CMD); // Initialise
    lcd_byte(0x06, LCD_CMD); // Cursor move direction
    lcd_byte(0x0C, LCD_CMD); // 0x0F On, Blink Off
    lcd_byte(0x28, LCD_CMD); // Data length, number of lines, font size
    lcd_byte(0x01, LCD_CMD); // Clear display
    delayMicroseconds(500);

    fd = wiringPiI2CSetup(I2C_ADDR);
}

void lcd_display_clear(void) {
  lcd_byte(0x01, LCD_CMD);
  lcd_byte(0x02, LCD_CMD);
}

void type_string(const char *s) {
  while ( *s ) lcd_byte(*(s++), LCD_CHR);
}

void type_float(float my_float) {
  char buffer[20];
  sprintf(buffer, "%.2f", my_float);
  type_string(buffer);
}

void lcd_byte(int bits, int mode) {
  // bits = the data / mode = 1 for data, 0 for command
  int bits_high;
  int bits_low;
  
  // uses the two half byte writes to LCD
  bits_high = mode | (bits & 0xF0) | LCD_BACKLIGHT ;
  bits_low = mode | ((bits << 4) & 0xF0) | LCD_BACKLIGHT ;

  // High bits
  wiringPiI2CReadReg8(fd, bits_high);
  lcd_toggle_enable(bits_high);

  // Low bits
  wiringPiI2CReadReg8(fd, bits_low);
  lcd_toggle_enable(bits_low);
}

void lcd_location(int line) {
  lcd_byte(line, LCD_CMD);
}

void lcd_toggle_enable(int bits) {
  delayMicroseconds(500);
  wiringPiI2CReadReg8(fd, (bits | ENABLE));
  delayMicroseconds(500);
  wiringPiI2CReadReg8(fd, (bits & ~ENABLE));
  delayMicroseconds(500);
}
