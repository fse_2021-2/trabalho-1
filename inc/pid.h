#ifndef PID_H_
#define PID_H_

/*
 *  Author: Renato Coral Sampaio
*/
void pid_configura_constantes(double Kp_, double Ki_, double Kd_);
void pid_atualiza_referencia(float referencia_);
double pid_controle(double saida_medida);

#endif
