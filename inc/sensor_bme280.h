#ifndef SENSOR_BME280_H_
#define SENSOR_BME280_H_

#include "bme280.h"

/**\
 * Biblioteca baseada no exemplo disponível no repositório do Bosch Sensortec BME280 sensor driver. 
 * (https://github.com/BoschSensortec/BME280_driver/blob/master/examples/linux_userspace.c)
 * 
 * Copyright (c) 2020 Bosch Sensortec GmbH. All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 **/

/* Realiza as configurações iniciais do sensor bme280 */
void sensor_bme280_init();

/* Retorna a temperatura ambiente lida pelo sensor bme280 */
float sensor_bme280_ambient_temperature();

/* Configura o sensor para o 'forced_mode' */
void set_sensor_data_forced_mode_settings(struct bme280_dev *dev);

/* Cria um delay obrigatório em algumas APIs */
void user_delay_us(uint32_t period, void *intf_ptr);

/* Leitura dos registros do sensor através do barramento I2C */
int8_t user_i2c_read(uint8_t reg_addr, uint8_t *data, uint32_t len, void *intf_ptr);

/* Escrita dos registrados do sensor através do barramento I2C */
int8_t user_i2c_write(uint8_t reg_addr, const uint8_t *data, uint32_t len, void *intf_ptr);

/* Lê a temperatura, umidade e pressão do sensor em 'forced mode' */
int8_t stream_sensor_data_forced_mode(struct bme280_dev *dev);

#endif