#ifndef GPIO_POWER_CIRCUIT_H_
#define GPIO_POWER_CIRCUIT_H_

/* Faz a configuração da biblioteca wiringPi, permitindo o uso dos atuadores */
void gpio_power_circuit_init();

/* Desliga os atuadores, setando os pinos para o modo INPUT */
void gpio_power_circuit_close();

/* Atualiza a temperatura de referência e cálculo o sinal de controle */
int calculate_control_signal(int, int);

/* Controla o acionamento dos atuadores (Resistor e Ventoinha) */
void power_circuit_control(int);

/* Configura as constantes do cálculo PID */
void configure_pid_constants(float, float, float);

#endif