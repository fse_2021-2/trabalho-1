#ifndef CONTROL_H_
#define CONTROL_H_

/* Enum com os modos de controle da temperatura de referência  */
enum reference_temperature_mode {POTENTIOMETER = 1, REFLOW, TERMINAL};

/* Enum com os estados do sistema */
enum system_state {OFF, ON};

/* Inicializa os módulos do sistema (UART, GPIO, I2C) */
void set_up_modules();

/* Encerra a comunicação com os módulos do sistema */
void set_down_modules();

/* Realiza as configurações iniciais do sistema */
void control_system_init();

/* Inicia o loop com o controle dos comandos do usuário do dashboard */
void control_system_start();

/* Realiza a rotina de controle de sistema de acordo com o modo de controle e 
    estado do sistema */
void control_system_routine();

/* Retorna a temperatura atual de acordo com a struct da curva de reflow */
float get_reflow_curve_temperature();

/* Configuração do sistema via Terminal. O usuário pode alterar
    o modo de controle e o valor das constantes do cálculo do PID */
void control_system_configuration();

/* Define o modo de controle da temperatura de referência via Terminal */
void user_set_control_mode();

/* Altera as constantes do controle PID via Terminal */
void user_set_pid_constants();

/* Retorna o nome do modo de controle atual */
char * get_actual_control_mode_name();

#endif