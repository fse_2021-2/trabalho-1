#ifndef UART_COMMUNICATION_H_
#define UART_COMMUNICATION_H_

/* Inicializa a comunicação via UART */
void uart_communication_init();

/* Encerra a comunicação via UART */
void uart_communication_close();

/* Inicializa o comando de comunicação com a UART, setando o servidor, 
    código e sub-código da mensagem, e a matrícula */
void command_init(int, int);

/* Envia o comando e faz a leitura do dado retornado via MODBUS */
void send_command_n_read_data();

/* Solicita a temperatura interna do sistema */
float request_intern_temperature();

/* Solicita a temperatura do potenciômetro ligado à ESP */
float request_potentiometer_temperature();

/* Lê os comandos do usuário via DASHBOARD */
int request_user_command();

/* Envia o sinal de controle */
void send_control_signal(int);

/* Envia a temperatura de referência */
void send_reference_temperature(float);

/* Envia o estado do sistema */
void send_system_state(char);

/* Envia o modo de controle */
void send_control_mode(char);

#endif