#ifndef UART_H_
#define UART_H_

#include <stddef.h>

/* Inicaliza a comunicação via UART e faz as configurações iniciais */
void uart_init();

/* Envia dados para a ESP via UART */
void uart_write(unsigned char *, size_t);

/* Lê dados da ESP via UART */
void uart_read(unsigned char *, size_t);

/* Encerra a comunicação via UART */
void uart_close();

#endif