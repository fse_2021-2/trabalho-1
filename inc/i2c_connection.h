#ifndef I2C_CONNECTION_H_
#define I2C_CONNECTION_H_

/* Inicializa o sensor bme280 e o display LCD */
void i2c_connection_init();

/* Encerra as comunições via I2C */
void i2c_connection_close();

/* Retorna a temperatura ambiente do sistema */
float get_ambient_temperature();

/* Apresenta "Sistema Desligado" no display LCD */
void lcd_show_system_state_off();

/* Apresenta no display LCD o modo de controle e as temperaturas medidas */
void lcd_show_measured_data(char *, float, float, float);

#endif