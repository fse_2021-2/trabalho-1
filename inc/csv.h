#ifndef CSV_H_
#define CSV_H_

#include <stdio.h>
#include <stdlib.h>

/* Struct com a estrutura da curva de reflow do arquivo csv */
typedef struct Reflow {
    int time;
    int temperature;
} Reflow;

/* Faz a leitura do arquivo com a curva de reflow */
Reflow * read_csv();

/* Cria um arquivo csv com o cabeçalho do log de dados */
FILE * create_csv_file();

/* Escreve uma linha de dados no arquivo csv */
void write_csv_row(FILE *, char *, float, float, float, int);

#endif