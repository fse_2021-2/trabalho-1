#ifndef MODBUS_H_
#define MODBUS_H_

#include <stdbool.h>
#include <stddef.h>

/* Calcula o CRC e envia o comando de solicitação de dados via UART */
void modbus_write(char *, size_t);

/* Faz a leitura da mensagem de retorno e realiza as validações */
bool modbus_read(char *, char *);

/* Valida o CRC da mensagem recebida */
bool validate_crc(int);

/* Valida o header da mensagem recebida */
bool validate_message_header(char *);

#endif