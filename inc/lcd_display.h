#ifndef LCD_DISPLAY_H_
#define LCD_DISPLAY_H_

/**\
 * Biblioteca baseada no exemplo disponível no repositório do enunciado do projeto. 
 * (https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-1-2021-2/-/blob/main/driver_lcd/control_lcd_16x2.c)
 * 
 * by Lewis Loflin www.bristolwatch.com lewis@bvu.net
 * http://www.bristolwatch.com/rpi/i2clcd.htm
 * Using wiringPi by Gordon Henderson
 **/

/* Inicializa o display LCD */
void lcd_display_init();

/* Limpa o display LCD */
void lcd_display_clear();

/* Escreve um string no display LCD */
void type_string(const char *);

/* Escreve um float no display LCD */
void type_float(float);

/* Envia um byte para o pino de dados do display */
void lcd_byte(int, int);

/* Vai para uma posição (linha) no LCD */
void lcd_location(int);

/* Altera pino 'enable' no display LCD */
void lcd_toggle_enable(int);

#endif