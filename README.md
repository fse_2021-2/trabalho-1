# FSE: Trabalho 1 - 2021.2.

**Nome:** Hérick Ferreira de Souza Portugues

**Matrícula:** 18/0033034

**Descrição:** Este trabalho refere-se ao definido em: https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-1-2021-2/-/tree/main

## Projeto

Ao iniciar o programa é apresentado um menu de utilização em que o usuário pode escolher entre 2 opções, sendo elas:  
   - **Configurar Sistema**
   - **Iniciar Sistema**

### Configurar Sistema

Ao selecionar a opção ***Configurar Sistema***, é apresentado ao usuário a possibilidade de configurar 2 aspectos do controle do sistema:
 - Alterar o modo de controle da temperatura de referência **(Padrão: Potenciômetro)**. Os modos disponíveis são:
    - Através de um Potenciômetro externo;
    - Através de entrada de teclado no terminal;
    - Seguindo uma curva de temperatura pré-definida em arquivo de configuração (Arquivo da Curva).
 - Alterar as constantes utilizadas para o cálculo do PID **(Padrão: Rasp43 -> Kp = 20.0, Ki = 0.1, Kd = 100.0)**.

As configurações definidas pelo usuário funcionam como uma configuração inicial do sistema, visto que após o início do controle, o usuário só poderá alterar o estado e modo de controle do sistema pelo dashboard, e não mais pelo terminal.

### Iniciar Sistema

Ao selecionar a opção ***Iniciar Sistema***, todas as comunicações com os diferentes módulos do sistema são estabelecidas e o controle do forno é iniciado. O controle se inicia com o sistema desligado e entra em um loop esperando por comandos do usuário via Dashboard. Assim que o usuário liga o sistema, um novo arquivo csv é criado e a rotina de controle das temperaturas (de acordo com o modo de controle definido pelo usuário via Terminal/Dashboard) do forno começa. 

A cada execução são obtidos dados das temperaturas de referência, interna e ambiente e calculado o sinal de controle (método PID) para que os atuadores possam ser acionados para aumentar (resistor) ou diminiuir (ventoinha) a temperatura do forno. Com isso, o display LCD é atualizado com os valores atuais do sistema e uma nova linha de log é adicionada ao arquivo csv.

## Experimentos

Ambos os experimentos foram realizados na Rasp43, com os valores das constante do PID definidos como: ***Kp = 20.0, Ki = 0.1, Kd = 100.0*** e com duração superior a 10 minutos (640 execuções).

O eixo x de todos os gráficos (Execução) representa uma execução do sistema de controle, que não necessariamente são espaçadas por intervalos de 1s (devido ao delay de comunicação entre os diferentes módulos do sistema). Os arquivos utilizados para os experimento (log da execução) encontram-se na pasta **data**:
- Curva de Reflow : *data/data_file_1646658419.csv*
- Potenciômetro: *data/data_file_1646682233.csv*

Os gráficos foram gerados em Python utilizando a biblioteca matplotlib.

### Experimento 1 - Curva de Reflow:

<figcaption align="center"><b>Fig. 1 - Gráfico das Temperaturas - Curva de Reflow</b></figcaption>
![Curva de Reflow - TEMP](https://cdn.discordapp.com/attachments/407697546606280714/950530580183920670/Screenshot_from_2022-03-07_20-02-18.png)

<figcaption align="center"><b>Fig. 2 - Gráfico do Sinal de Controle - Curva de Reflow</b></figcaption>
![Curva de Reflow - CONTROL](https://cdn.discordapp.com/attachments/407697546606280714/950532677222694922/Screenshot_from_2022-03-07_20-16-45.png)

### Experimento 2 - Potenciômetro:

<figcaption align="center"><b>Fig. 3 - Gráfico das Temperaturas - Potenciômetro</b></figcaption>
![Potenciômetro - TEMP](https://cdn.discordapp.com/attachments/407697546606280714/950530579814830100/Screenshot_from_2022-03-07_20-04-21.png)

<figcaption align="center"><b>Fig. 4 - Gráfico do Sinal de Controle - Potenciômetro</b></figcaption>
![Potenciômetro - CONTROL](https://cdn.discordapp.com/attachments/407697546606280714/950532677012951091/Screenshot_from_2022-03-07_20-17-08.png)

## Módulos do Sistema

O código do projeto está dividido em 13 "módulos" e a comunicação entre eles, assim como uma breve explicação de cada um, pode ser visto abaixo:

<figcaption align="center"><b>Fig. 5 - Modularização do sistema</b></figcaption>
![Trabalho 1](https://cdn.discordapp.com/attachments/407697546606280714/950497025437343784/FSE.png)

- **main**: Início da execucação do programa, contando com o menu inicial.

- **control**: Camada responsável pelo controle do sistema, é nela que os estados do sistema são armazenados, as configurações do controle são feitas e todas as conexões são estabelecidas.

- **csv**: Responsável pela criação, escrita e leitura dos arquivos csv.

- **uart_communication**: Camada responsável pela comunicação com a ESP32 via UART (Protocolo MODBUS-RTU).

- **modbus**: Camada responsável pela implementação do protocolo MODBUS-RTU para comunicação via UART.

- **crc16**: Realiza o cálculo do CRC. Camada implementada pelo professor.

- **uart**: Responsável pela configuração, leitura e escrita da comunicação UART.

- **gpio_power_circuit**: Camada responśavel pela comunicação via GPIO com o circuito de potência, controlando o acionamento dos atuadores (Resistor e Ventoinha).

- **pid**: Responsável pelo cálculo do PID. Camada implementada pelo professor.

- **i2c_connection**: Camada responsável pela comunicação via I2C com o sensor bme280 e com o display lcd.

- **sensor_bme280**: Responsável pela configuração e leitura dos dados do sensor bme280. Baseado em: https://github.com/BoschSensortec/BME280_driver/blob/master/examples/linux_userspace.c.

- **bme280**: Driver da Bosch Sensortec GmbH para o sensor BME280.

- **lcd_display**: Responsável pela configuração e escrita do display LCD. Baseado em: https://gitlab.com/fse_fga/trabalhos-2021_2/trabalho-1-2021-2/-/blob/main/driver_lcd/control_lcd_16x2.c.

## Rodando o Programa

Para compilar e rodar o programa, devem ser utilizados os seguintes comandos via terminal na pasta do projeto: 
```
make
make run
```

## Observações
 - Este programa é compatível com o sistema operacional Linux;
 - O código conta com comentários nos arquivos .h com uma breve explicação de cada uma das funções, além de comentários pontuais nos arquivos .c;
 - O sinal SIGINT encerra a comunicação com os módulos do sistema e encerra a execução do programa;
 - Na camada uart_communication, a escrita/leitura dos dados via UART é realizada por até 3 vezes (em caso de erro no CRC/header). Caso a comunicação não tenha sucesso nessas tentativas, é entendido que há problema na comunicação com a ESP e o programa é encerrado.
